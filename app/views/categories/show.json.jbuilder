json.extract! @category, :id, :name, :excerpt, :description, :seo_title, :seo_keywords, :seo_description, :created_at, :updated_at

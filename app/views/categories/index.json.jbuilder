json.array!(@categories) do |category|
  json.extract! category, :id, :name, :excerpt, :description, :seo_title, :seo_keywords, :seo_description
  json.url category_url(category, format: :json)
end

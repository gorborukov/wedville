class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.string :excerpt
      t.text :description
      t.string :seo_title
      t.string :seo_keywords
      t.string :seo_description

      t.timestamps
    end
  end
end

class Category < ActiveRecord::Base
  has_attached_file :image, :styles => { :original => "210x210#", :thumb => "100x100#", :mini => "64x64#", :nano => "20x20#" }, :default_url => "/assets/missing.png"
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
end
